package models.db.questionsData;

import models.db.QuestionBaseModel;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by admin on 19-Dec-14.
 */
@Entity
@Table(name="open_text_data")
public class OpenTextData extends QuestionBaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OpenTextData(){
    }

    public OpenTextData(String elementType, String elementName) {
        this.elementType = elementType;
        this.elementName = elementName;
    }
}
