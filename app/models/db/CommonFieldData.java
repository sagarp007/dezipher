package models.db;

import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Created by admin on 19-Dec-14.
 */
@Entity
@Table(name="common_field_data")
public class CommonFieldData extends QuestionBaseModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CommonFieldData() {
    }

    public CommonFieldData(String elementType, String elementName) {
        this.elementType = elementType;
        this.elementName = elementName;
    }

    public static Finder<Long, CommonFieldData> find = new Finder<Long, CommonFieldData>(
            Long.class, CommonFieldData.class
    );
}
