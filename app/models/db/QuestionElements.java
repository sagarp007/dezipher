package models.db;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by admin on 23-Jan-15.
 */
@Entity
@Table(name="question_elements")
public class QuestionElements extends Model{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public String questionType;

    public String elementType;

    public String elementName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public QuestionElements() {
    }

    public QuestionElements(String questionType, String elementType, String elementName) {
        this.questionType = questionType;
        this.elementType = elementType;
        this.elementName = elementName;
    }

    public static Finder<Long, QuestionElements> find = new Finder<Long, QuestionElements>(
            Long.class, QuestionElements.class
    );
}
