package models.standards;

/**
 * Created by admin on 16-Dec-14.
 */
public class SurveyStandards {

    public int fieldWidth;

    public String validations;

    public String required;

    public String defaultQText;

    public String defaultQInstruction;

    public String recoding;

    public String dropOut;

    public String hNoQ;

    public String qTitleFormat;

    public int getFieldWidth() {
        return fieldWidth;
    }

    public void setFieldWidth(int fieldWidth) {
        this.fieldWidth = fieldWidth;
    }

    public String getValidations() {
        return validations;
    }

    public void setValidations(String validations) {
        this.validations = validations;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getDefaultQText() {
        return defaultQText;
    }

    public void setDefaultQText(String defaultQText) {
        this.defaultQText = defaultQText;
    }

    public String getDefaultQInstruction() {
        return defaultQInstruction;
    }

    public void setDefaultQInstruction(String defaultQInstruction) {
        this.defaultQInstruction = defaultQInstruction;
    }

    public String getRecoding() {
        return recoding;
    }

    public void setRecoding(String recoding) {
        this.recoding = recoding;
    }

    public String getDropOut() {
        return dropOut;
    }

    public void setDropOut(String dropOut) {
        this.dropOut = dropOut;
    }

    public String gethNoQ() {
        return hNoQ;
    }

    public void sethNoQ(String hNoQ) {
        this.hNoQ = hNoQ;
    }

    public String getqTitleFormat() {
        return qTitleFormat;
    }

    public void setqTitleFormat(String qTitleFormat) {
        this.qTitleFormat = qTitleFormat;
    }
}
