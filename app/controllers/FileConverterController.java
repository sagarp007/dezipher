package controllers;

import com.avaje.ebean.Ebean;
/*import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.util.JSONPObject;
import models.db.CommonFieldData;
import models.db.QuestionBaseModel;*/
import models.db.QuestionElements;
/*import models.db.questionsData.*;*/
import models.standards.SurveyStandards;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
/*import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;*/
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.docx4j.convert.out.html.AbstractHtmlExporter;
import org.docx4j.convert.out.html.HtmlExporterNG2;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import play.api.templates.Html;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import scala.util.parsing.json.JSONArray;
//import scala.util.parsing.json.JSONObject;
//import scala.util.parsing.json.JSONObject;

import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by admin on 16-Dec-14.
 */
public class FileConverterController extends Controller {
    public static Result getHtml() throws Exception {

        String htmlString = "";
        Map<String, String[]> map = request().body().asMultipartFormData().asFormUrlEncoded();
        if (map == null)
            return badRequest();
        Http.MultipartFormData body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart fp = body.getFile("ffp");
        String  fieldWidth = StringUtils.isEmpty(map.get("fieldWidth")[0]) ? "0" : map.get("fieldWidth")[0];
        String  validations = StringUtils.isEmpty(map.get("validations")[0]) ? "0" : map.get("validations")[0];
        String  required = StringUtils.isEmpty(map.get("required")[0]) ? "0" : map.get("required")[0];
        String  defaultQText = StringUtils.isEmpty(map.get("defaultQText")[0]) ? "0" : map.get("defaultQText")[0];
        String  defaultQInstruction = StringUtils.isEmpty(map.get("defaultQInstruction")[0]) ? "0" : map.get("defaultQInstruction")[0];
        String  recoding = StringUtils.isEmpty(map.get("recoding")[0]) ? "0" : map.get("recoding")[0];
        String  dropOut = StringUtils.isEmpty(map.get("dropOut")[0]) ? "0" : map.get("dropOut")[0];
        String  hNoQ = StringUtils.isEmpty(map.get("hNoQ")[0]) ? "0" : map.get("hNoQ")[0];
        String  qTitleFormat = StringUtils.isEmpty(map.get("qTitleFormat")[0]) ? "0" : map.get("qTitleFormat")[0];

        SurveyStandards surveyStandards = new SurveyStandards();
        surveyStandards.setFieldWidth(Integer.parseInt(fieldWidth));
        surveyStandards.setValidations(validations);
        surveyStandards.setRequired(required);
        surveyStandards.setDefaultQText(defaultQText);
        surveyStandards.setDefaultQInstruction(defaultQInstruction);
        surveyStandards.setRecoding(recoding);
        surveyStandards.setDropOut(dropOut);
        surveyStandards.sethNoQ(hNoQ);
        surveyStandards.setqTitleFormat(qTitleFormat);

        //FileInputStream fs = new FileInputStream(fp.getFile());
        //htmlString = createHTML(fp.getFile());
        List<QuestionElements> questionsData;
        JSONObject questions = getQuestionsData();
        String questionsString = questions.toString();
        //JSONObject json = new JSONObject();
        //json.putAll( questionsData );
        htmlString = createHTML2(fp.getFile());
        htmlString = StringEscapeUtils.unescapeHtml4(htmlString);
        List<QuestionElements> qbList = Ebean.find(QuestionElements.class).findList();
        //play.Logger.info("its the questionsData " + questions);
        //play.Logger.info("This is questionsString " + questionsString);
        //questionsString = questionsString.replaceAll("\"","\\\"");
        //questionsString = questionsString.replace("\\\\", "");
        //questionsString = questionsString.replaceAll("'", "\\\\u0022");
        //play.Logger.info("++++++++Json.toJson(questionsString)++++++++++++"+Json.toJson(questionsString));
        return ok(views.html.interfaceView. save.render("Interface Editor", qbList, surveyStandards, Html.apply(htmlString.replace("\"", "'"))));
    }

    public static JSONObject getQuestionsDetails(){
        List<QuestionElements> qbList = null;
        JSONObject questions = new JSONObject();
        JSONObject questionElement = null;


        try {
            qbList = Ebean.find(QuestionElements.class).findList();
            JSONObject obj = new JSONObject();

            for(QuestionElements qb: qbList){
                questionElement = new JSONObject();
                questionElement.put("questionType",qb.getQuestionType());
                questionElement.put("elementType",qb.getElementType());
                questionElement.put("elementName",qb.getElementName());
                questions.put("",questionElement) ;
            }
        } catch (Exception ex){
            play.Logger.error(ex.getMessage());
        }
        return questions;
    }

    public static JSONObject getQuestionsData(){
        List<QuestionElements> qbList = null;
        JSONObject questions = new JSONObject();
        JSONObject questionElement = null;
        org.json.JSONArray commonDataArray = new org.json.JSONArray();
        questions.put("common",commonDataArray);
        org.json.JSONArray singleQArray = new org.json.JSONArray();
        questions.put("single",singleQArray);
        org.json.JSONArray multiQArray = new org.json.JSONArray();
        questions.put("multi",multiQArray);
        org.json.JSONArray gridQArray = new org.json.JSONArray();
        questions.put("grid",gridQArray);
        org.json.JSONArray multiGridQArray = new org.json.JSONArray();
        questions.put("multiGrid",multiGridQArray);
        org.json.JSONArray dGridQArray = new org.json.JSONArray();
        questions.put("dGrid",dGridQArray);
        org.json.JSONArray openTextQArray = new org.json.JSONArray();
        questions.put("openText",openTextQArray);
        org.json.JSONArray openTextListQArray = new org.json.JSONArray();
        questions.put("openTextList",openTextListQArray);
        org.json.JSONArray numericQArray = new org.json.JSONArray();
        questions.put("numeric",numericQArray);
        org.json.JSONArray numericListQArray = new org.json.JSONArray();
        questions.put("single",numericListQArray);
        org.json.JSONArray rankingQArray = new org.json.JSONArray();
        questions.put("single",rankingQArray);
        //org.json.JSONArray questionsArray = new org.json.JSONArray();

        try {
            qbList = Ebean.find(QuestionElements.class).findList();
            JSONObject obj = new JSONObject();

            for(QuestionElements qb: qbList){
                questionElement = new JSONObject();
                if(qb.getQuestionType().contains("common")){
                    questionElement.put("elementType",qb.getElementType());
                    questionElement.put("elementName",qb.getElementName());
                    commonDataArray.put(questionElement);
                }
                else if(qb.getQuestionType().contains("single")){
                    questionElement.put("elementType",qb.getElementType());
                    questionElement.put("elementName",qb.getElementName());
                    singleQArray.put(questionElement);
                }
                else if(qb.getQuestionType().contains("multi")){
                    questionElement.put("elementType",qb.getElementType());
                    questionElement.put("elementName",qb.getElementName());
                    multiQArray.put(questionElement);
                }
                else if(qb.getQuestionType().contains("openText")){
                    questionElement.put("elementType",qb.getElementType());
                    questionElement.put("elementName",qb.getElementName());
                    openTextQArray.put(questionElement);
                }
                else if(qb.getQuestionType().contains("numeric")){
                    questionElement.put("elementType",qb.getElementType());
                    questionElement.put("elementName",qb.getElementName());
                    numericQArray.put(questionElement);
                }
                else if(qb.getQuestionType().contains("openTextList")){
                    questionElement.put("elementType",qb.getElementType());
                    questionElement.put("elementName",qb.getElementName());
                    openTextListQArray.put(questionElement);
                }
                else if(qb.getQuestionType().contains("numericList")){
                    questionElement.put("elementType",qb.getElementType());
                    questionElement.put("elementName",qb.getElementName());
                    numericListQArray.put(questionElement);
                }
                else if(qb.getQuestionType().contains("grid")){
                    questionElement.put("elementType",qb.getElementType());
                    questionElement.put("elementName",qb.getElementName());
                    gridQArray.put(questionElement);
                }
                else if(qb.getQuestionType().contains("multiGrid")){
                    questionElement.put("elementType",qb.getElementType());
                    questionElement.put("elementName",qb.getElementName());
                    multiGridQArray.put(questionElement);
                }
                else if(qb.getQuestionType().contains("3dGrid")){
                    questionElement.put("elementType",qb.getElementType());
                    questionElement.put("elementName",qb.getElementName());
                    dGridQArray.put(questionElement);
                }
                else if(qb.getQuestionType().contains("ranking")){
                    questionElement.put("elementType",qb.getElementType());
                    questionElement.put("elementName",qb.getElementName());
                    rankingQArray.put(questionElement);
                }
            }
        } catch (Exception ex){
            play.Logger.error(ex.getMessage());
        }
        return questions;
    }

    /****************Generate html using XDOCReport****************/
    public static String createHTML(File file) throws Exception{
        InputStream is = null;
        OutputStream out = null;
        File temp = null;
        String htmlString = "";
        try {
            long start = System.currentTimeMillis();

            // 1) Load DOCX into XWPFDocument
            is = new FileInputStream(file);
            XWPFDocument document = new XWPFDocument(is);

            // 2) Prepare Html options
            XHTMLOptions options = XHTMLOptions.create();
            // 3) Convert XWPFDocument to HTML
            temp = File.createTempFile("HelloWorld", ".html");
            out = new FileOutputStream(temp);
            XHTMLConverter.getInstance().convert(document, out, options);

            htmlString = getHTMLContent(temp);
            System.err.println("Generate html/HelloWorld.html with " + (System.currentTimeMillis() - start) + "ms");

        } catch (Throwable e) {
            e.printStackTrace();
        }finally {
            if (is!=null)
                is.close();
            if (out!=null)
                out.close();
            if (temp!=null)
                temp.delete();
        }
        return htmlString;
    }

    /****************Generate html using DOCX4j****************/
    public static String createHTML2(File file) throws Exception{
        InputStream is = null;
        OutputStream out = null;
        File temp = null;
        String htmlString = "";
        try {
            long start = System.currentTimeMillis();

            // 1) Load DOCX into WordprocessingMLPackage
            is = new FileInputStream(file);
            WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(is);
            //PStyle style = new PStyle();
            // 2) Prepare HTML settings
            AbstractHtmlExporter.HtmlSettings htmlSettings = new AbstractHtmlExporter.HtmlSettings();
            // 3) Convert WordprocessingMLPackage to HTML
            temp = File.createTempFile("HelloWorld", ".html");
            out = new FileOutputStream(temp);
            AbstractHtmlExporter exporter = new HtmlExporterNG2();
            StreamResult result = new StreamResult(out);
            exporter.html(wordMLPackage, result, htmlSettings);
            htmlString = getHTMLContent(temp);
            System.err.println("Generate html/HelloWorld.html with " + (System.currentTimeMillis() - start) + "ms");

        } catch (Throwable e) {
            e.printStackTrace();
        }finally {
            if (is!=null)
                is.close();
            if (out!=null)
                out.close();
            if (temp!=null)
                temp.delete();

        }
        return htmlString;
    }

    public static String getHTMLContent(File file) throws Exception{
        BufferedReader br = null;
        String selectedHtmlAsString = "";
        String cssString = "";
        try {
            br = new BufferedReader(new FileReader(file));
            String line;
            StringBuffer sb = new StringBuffer();

            // read contents line by line and store in the string
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            org.jsoup.nodes.Document doc = Jsoup.parse(sb.toString());
            doc.outputSettings().charset("ISO-8859-1");
            Elements links = doc.select("td");
            links.attr("style", "border:1px solid;");
            //doc.outputSettings().
            /*****For XDOCReport****/
            Elements wordSection = doc.getElementsByTag("div");
            /*****For DOCX4j*****/
            //Elements wordSection = doc.select("div.document");
            Elements stylesSection = doc.getElementsByTag("style");
            cssString = stylesSection.html();
            selectedHtmlAsString = wordSection.html();
            selectedHtmlAsString = "<style>" + cssString + "</style>" + selectedHtmlAsString;
            //System.out.println(selectedHtmlAsString);

        }catch (Exception ex){
            play.Logger.info(ex.getMessage());
        }finally {
            if (br!=null)
                br.close();

        }
        return selectedHtmlAsString;
    }
}
