import com.avaje.ebean.Ebean;
import models.db.CommonFieldData;
import models.db.QuestionElements;
import play.Application;
import play.GlobalSettings;
import play.libs.Yaml;

import java.util.List;
import java.util.Map;

/**
 * Created by admin on 23-Jan-15.
 */
public class Global extends GlobalSettings {
    @Override
    public void onStart(Application app) {
        // Check if the database is empty
        if (QuestionElements.find.findRowCount() == 0) {
            @SuppressWarnings("unchecked")
            Map<String, List<Object>> all  = (Map<String, List<Object>>) Yaml.load("fixtures/question_type/initial-data.yml");
            Ebean.save(all.get("commonFields"));
            Ebean.save(all.get("singleQuestions"));
            Ebean.save(all.get("multiQuestions"));
            Ebean.save(all.get("openTextQuestions"));
            Ebean.save(all.get("numericQuestions"));
            Ebean.save(all.get("openTextList"));
            Ebean.save(all.get("numericList"));
            Ebean.save(all.get("grid"));
            Ebean.save(all.get("multiGrid"));
            Ebean.save(all.get("3DGrid"));
            Ebean.save(all.get("ranking"));
            //Ebean.save((List) Yaml.load("fixtures/question_type/initial-data.yml"));
        }
    }
}
