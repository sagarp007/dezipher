name := "dezipher"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  "postgresql" % "postgresql" % "9.1-901.jdbc4",
  "org.apache.poi" % "poi-ooxml" % "3.9",
  "commons-io" % "commons-io" % "2.3",
  "org.apache.commons" % "commons-lang3" % "3.0",
  "org.jsoup"%"jsoup"%"1.8.1",
  "fr.opensagres.xdocreport"%"xdocreport"%"1.0.4",
  "fr.opensagres.xdocreport" % "org.apache.poi.xwpf.converter.xhtml" % "1.0.4",
  "org.docx4j"%"docx4j"%"3.2.1",
  "org.json" % "json" % "20140107"
)     

play.Project.playJavaSettings
