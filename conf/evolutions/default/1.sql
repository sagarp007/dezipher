# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table common_field_data (
  id                        bigint not null,
  element_type              varchar(255),
  element_name              varchar(255),
  constraint pk_common_field_data primary key (id))
;

create table dgrid_data (
  id                        bigint not null,
  element_type              varchar(255),
  element_name              varchar(255),
  constraint pk_dgrid_data primary key (id))
;

create table grid_data (
  id                        bigint not null,
  element_type              varchar(255),
  element_name              varchar(255),
  constraint pk_grid_data primary key (id))
;

create table multi_data (
  id                        bigint not null,
  element_type              varchar(255),
  element_name              varchar(255),
  constraint pk_multi_data primary key (id))
;

create table multi_grid_data (
  id                        bigint not null,
  element_type              varchar(255),
  element_name              varchar(255),
  constraint pk_multi_grid_data primary key (id))
;

create table numeric_data (
  id                        bigint not null,
  element_type              varchar(255),
  element_name              varchar(255),
  constraint pk_numeric_data primary key (id))
;

create table numeric_list_data (
  id                        bigint not null,
  element_type              varchar(255),
  element_name              varchar(255),
  constraint pk_numeric_list_data primary key (id))
;

create table open_text_data (
  id                        bigint not null,
  element_type              varchar(255),
  element_name              varchar(255),
  constraint pk_open_text_data primary key (id))
;

create table open_text_list_data (
  id                        bigint not null,
  element_type              varchar(255),
  element_name              varchar(255),
  constraint pk_open_text_list_data primary key (id))
;

create table question_elements (
  id                        bigint not null,
  question_type             varchar(255),
  element_type              varchar(255),
  element_name              varchar(255),
  constraint pk_question_elements primary key (id))
;

create table ranking_data (
  id                        bigint not null,
  element_type              varchar(255),
  element_name              varchar(255),
  constraint pk_ranking_data primary key (id))
;

create table single_data (
  id                        bigint not null,
  element_type              varchar(255),
  element_name              varchar(255),
  constraint pk_single_data primary key (id))
;

create sequence common_field_data_seq;

create sequence dgrid_data_seq;

create sequence grid_data_seq;

create sequence multi_data_seq;

create sequence multi_grid_data_seq;

create sequence numeric_data_seq;

create sequence numeric_list_data_seq;

create sequence open_text_data_seq;

create sequence open_text_list_data_seq;

create sequence question_elements_seq;

create sequence ranking_data_seq;

create sequence single_data_seq;




# --- !Downs

drop table if exists common_field_data cascade;

drop table if exists dgrid_data cascade;

drop table if exists grid_data cascade;

drop table if exists multi_data cascade;

drop table if exists multi_grid_data cascade;

drop table if exists numeric_data cascade;

drop table if exists numeric_list_data cascade;

drop table if exists open_text_data cascade;

drop table if exists open_text_list_data cascade;

drop table if exists question_elements cascade;

drop table if exists ranking_data cascade;

drop table if exists single_data cascade;

drop sequence if exists common_field_data_seq;

drop sequence if exists dgrid_data_seq;

drop sequence if exists grid_data_seq;

drop sequence if exists multi_data_seq;

drop sequence if exists multi_grid_data_seq;

drop sequence if exists numeric_data_seq;

drop sequence if exists numeric_list_data_seq;

drop sequence if exists open_text_data_seq;

drop sequence if exists open_text_list_data_seq;

drop sequence if exists question_elements_seq;

drop sequence if exists ranking_data_seq;

drop sequence if exists single_data_seq;

