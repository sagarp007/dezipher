
    /*$(document).ready(function() {
        $('a.question_button').click(function(e) {
            var idValue = $(this).attr('id');
            showType(idValue,commonArray);
        });
        $('#act_short_keys').bind('click', bindt(commonArray));
        $('#de_act_short_keys').bind('click', unBindt);
    });*/

    var questionKeys = [
        {"key":"i","keyValue":"info_key"},
        {"key":"s","keyValue":"single_key"},
        {"key":"m","keyValue":"multi_key"},
        {"key":"o","keyValue":"openText_key"},
        {"key":"n","keyValue":"numeric_key"},
        {"key":"e","keyValue":"openTextList_key"},
        {"key":"u","keyValue":"numericList_key"},
        {"key":"r","keyValue":"ranking_key"},
        {"key":"g","keyValue":"grid_key"},
        {"key":"x","keyValue":"multiGrid_key"},
        {"key":"3","keyValue":"dGrid_key"},
        {"key":"l","keyValue":"reusableList_key"},
    ];

    /*function getQuestions(commonArr){
               var commonArray = [];
               var questionSelected;
               var displayFields = [];
               commonArray = commonArr;
               $('a.question_button').click(function(e) {
                   var idValue = $(this).attr('id');
                   showType(idValue,commonArray);
               });
           *//*jQuery(document).bind("keydown", function(event) {
               var idValue;
               var keycode = (event.keyCode ? event.keyCode : event.which);
               //("keycode si this "+keycode);
               $.each(questionKeys, function(i, item) {
                   if(questionKeys[i].key == String.fromCharCode(event.keyCode).toLowerCase()){
                       //console.log("questionKeys[i].keyValue" + questionKeys[i].keyValue);
                       idValue = questionKeys[i].keyValue;
                       //console.log("idValue in the loop " + idValue);
                       showType(idValue,commonArray);
                       return false;
                   }
               })
               //console.log("idValue before calling getType() " + idValue);
               //getType(idValue,commonArray);

           });*//*
           }*/

    function showType(idValue,commonArray){
        //alert("idValue " + idValue );
        if(idValue!='reusableList_button'){
            $('a.question_button').css('pointer-events', 'none');
            $('a.question_button').css('cursor', 'default');
            $('.common_field').css('display', 'inline-block');
            //var idValue = $(this).attr('id');
            //console.log("idValue"+idValue);
            if(idValue=='info_button'||idValue=='info_key'){
                $('.save_question').css('display', 'inline-block');
            }else if(idValue!='info_button' && idValue!='reusableList_button'){
                var idName = idValue.split('_');
                questionSelected = idName[0];
               // console.log("questionSelected: "+questionSelected);
                //console.log("id ====> "+ id);
                //return false;
                displayFields = new Array();
                $.each(commonArray, function(index, val) {
                    if(val.type == questionSelected && val.qElementType=="field"){
                       displayFields.push(val.qElementName);
                    }
                });
                //console.log("displayFields.length-------->" + displayFields.length);
                var i = 0;
                var fieldIdValue;
                do {
                    //console.log("displayFields"+displayFields[i]);
                    //console.log("$(displayFields[i].toString())"+displayFields[i].toString());
                    var str1 = displayFields[i].toString().toLowerCase();
                    //console.log("str1"+str1);
                    var tempIdField = str1;
                    //console.log("tempIdField => this may have spaces"+ tempIdField);
                    var tempId = tempIdField.split(' ');//tempId[0]=ans tempId[1]=list
                    //console.log("tempId[0] => this splits things"+ tempId[0]);
                    var j = 0;
                    var tempIdArray = new Array();
                    fieldIdValue='';
                    do {
                        tempIdArray = tempId[j];
                        //console.log("tempId[j]------>"+tempId[j]);
                         if(fieldIdValue=='')
                            fieldIdValue = tempIdArray;//fieldIdValue = ans; fieldIdValue = anslist
                         else
                            fieldIdValue = fieldIdValue + '_' + tempIdArray;
                        //var k = 0;
                        //console.log("fieldIdValue  ......."+fieldIdValue);
                        j++;
                    }while(j < tempId.length)
                    fieldIdValue = fieldIdValue + '_field';
                    //console.log("fieldIdValue  ......."+fieldIdValue);
                    $('#'+fieldIdValue).css('display', 'inline-block');
                    i++;
                }while(i < displayFields.length)
                $('.save_question').css('display', 'inline-block');
                //console.log("displayFields[0]====>>>>>"+displayFields[0]);
            }
        }
    }
     function saveQuestion(){
         $('.question_field').css('display', 'none');
         $('a.question_button').css('pointer-events', 'auto');
         $('a.question_button').css('cursor', 'pointer');
         //$('.question_button').unbind();
     }
     function cancelQuestion(){
         $('.question_field').css('display', 'none');
         $('a.question_button').css('pointer-events', 'auto');
         $('a.question_button').css('cursor', 'pointer');
         //$('.question_button').unbind();
     }

     function getSelectionText() {
         var html = "";
         if (typeof window.getSelection != "undefined") {
             var sel = window.getSelection();
             if (sel.rangeCount) {
                 var alert1 = "";
                 var container = document.createElement("div");
                 for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                     container.appendChild(sel.getRangeAt(i).cloneContents());
                     alert1 = alert1 +'<p>'+sel.getRangeAt(i)+'</p>';
                 }
                 html = alert1;
             }
         }
         return html;
     }

     /***************for selecting paras html****************/

     function getSelectionParagraph(){
         if (typeof window.getSelection != "undefined") {
              var sel1 = rangy.getSelection();
              var selectedText = rangy.getSelection().toHtml();
              console.log(" selectedText : "+selectedText);
              /*if (sel.rangeCount) {
                  var alert1 = "";
                  var container = document.createElement("div");
                  var asach = rangy.getSelection().toHtml();
                  alert(" for qText "+asach);
                  *//*for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                      asach = rangy.getSelection().toHtml();
                      alert1 = alert1 +'<p>'+sel.getRangeAt(i)+'</p>';
                  }*//*
              }*/
         }
         return selectedText;
     }

     /*******************************/

    /***************========for ans list selection========****************/
    /*****The getSelectionHtmlList function gives an array***********
    ******of ansListItem(s) which is an array of selected***********
    ******html ranges*********************************************/
    var htmlarr1;
    function getSelectionHtmlList() {
          var htmlPara = [];
          var htmlString = "";
          var selectedText;
          if (typeof window.getSelection != "undefined") {
              var sel = rangy.getSelection();
              var paraCount = 0;
              if (sel.rangeCount) {
                  var container = document.createElement("div");
                  for (var i = 0, len = sel.rangeCount; i < len; i++) {
                      container.appendChild(sel.getRangeAt(i).cloneContents());
                      selectedText = rangy.getSelection().toHtml();
                      console.log("selectedText "+selectedText);
                      if(selectedText.toLowerCase().indexOf("<td ") >= 0)
                        htmlarr1 = selectedText.split('</td>');
                      else{
                        alert("didn't got td");
                        htmlPara[paraCount] = "<span class='ansListItem'>"+sel.getRangeAt(i)+'</span>';
                        paraCount++;
                      }
                  }
                  if (htmlPara.length == 0) {
                      $.each( htmlarr1, function( key, value ) {
                          if(value.toLowerCase().indexOf("<td ") >= 0)
                                htmlarr1[key] = value + "</td>";
                                //value = "<span class='ansListItem'>"+value+"</span>";
                      });

                      $.each( htmlarr1, function( key, value ) {
                         // console.log(" changed value: "+value);
                          //alert("value : "+value);
                      });
                  }

              }
          }
          if(htmlPara.length != 0)
            htmlString = htmlPara;
          else
            htmlString = htmlarr1;
          return htmlString;
    }

    /****************************************/


    /***************************************/
        var retainClassArray = ["ansListItem", "ans_option_span", "ans_del_span","ans_code_span"];
    function formatForNow(){
        $('li.ans_row').find('*').each(function() {
            var attributes = $.map(this.attributes, function(item) {
                return item.name;
              });
            var fontWeight = $(this).css('font-weight');
            var fontStyle = $(this).css('font-style');
            var textDecoration = $(this).css('text-decoration');
            var className = $(this).attr('class');

              // now use jQuery to remove the attributes
              var img = $(this);
              $.each(attributes, function(i, item) {
                img.removeAttr(item);
              });

              if(className){
                var i;
                for (i = 0; i < retainClassArray.length; ++i) {
                    if(className==retainClassArray[i])
                    $(this ).addClass(retainClassArray[i]);
                }
                /*if(className=="ansListItem"||className=="ans_option_span"||className=="ans_del_span"||className=="ans_code_span"){
                    $(this ).attr("class","ansListItem");
                }*/
              }

              if(fontWeight){
                if (fontWeight  == 'bold' || fontWeight  == '700'){
                    //alert("got something 2");
                    //$(this ).attr("style","font-weight:bold");
                     $(this ).addClass("bold");
                }
              }

              if(fontStyle){
                if (fontStyle  == 'italic'){
                    //alert("got something 3");
                    //$(this ).css("font-style","italic");
                    $(this ).addClass("italic");
                }
              }
              if(textDecoration){
                if (textDecoration == 'underline'){
                    //alert("got something 4");
                    //$(this ).css("text-decoration","underline");
                    $(this ).addClass("underline");
                }
              }
        });
    }
    /***************************************/


    /*****************resetting html for bold,italics, underline, className*****************/
    function formatAns(fieldID){
        $('#'+fieldID).find('*').each(function() {
            var attributes = $.map(this.attributes, function(item) {
                return item.name;
              });
            var fontWeight = $(this).css('font-weight');
            var fontStyle = $(this).css('font-style');
            var textDecoration = $(this).css('text-decoration');
            var className = $(this).attr('class');

              // now use jQuery to remove the attributes
              var img = $(this);
              $.each(attributes, function(i, item) {
                img.removeAttr(item);
              });

              if(className){
                if(className=="ansListItem"){
                    $(this ).attr("class","ansListItem");
                }
              }

              if(fontWeight){
                if (fontWeight  == 'bold' || fontWeight  == '700'){
                    //alert("got something 2");
                    $(this ).attr("style","font-weight:bold");
                }
              }

              if(fontStyle){
                if (fontStyle  == 'italic'){
                    //alert("got something 3");
                    $(this ).css("font-style","italic");
                }
              }
              if(textDecoration){
                if (textDecoration == 'underline'){
                    //alert("got something 4");
                    $(this ).css("text-decoration","underline");
                }
              }
        });


        /*$('#ansList').find('*').each(function() {
            var boldvalue = $( this ).attr("style");
            var boldvalue1 = $(this).attr("style");
            var fontWeight = $(this).css('font-weight');
            var fontStyle = $(this).css('font-style');
            var textDecoration = $(this).css('text-decoration');
            var attributes = $.map(this.attributes, function(item) {
                return item.name;
            });


              // now use jQuery to remove the attributes
              var img = $(this);
              $.each(attributes, function(i, item) {
                img.removeAttr(item);
              });

            if (fontWeight  == 'bold' || fontWeight  == '700'){
                //alert("got something");
                $(this ).attr("style","font-weight:bold");
            }
            if (fontStyle  == 'italic'){
                //alert("got something");
                $(this ).css("font-style","italic");
            }
            if (textDecoration == 'underline'){
                //alert("got something");
                $(this ).css("text-decoration","underline");
            }

            // New type of the tag
            var replacementTag = 'span';
           // alert("outside func to remove td tag");
            var divHtml = $('#ansList').innerHTML;
            //alert("divHtml ==> "+divHtml);
            // Replace all a tags with the type of replacementTag
            $('#ansList td').each(function() {
            alert("inside func to remove td tag");
                var outer = this.outerHTML;
                console.log(" outer html " + outer);
                // Replace opening tag
                var regex = new RegExp('<' + this.tagName, 'i');
                var newTag = outer.replace(regex, '<' + replacementTag);

                // Replace closing tag
                regex = new RegExp('</' + this.tagName, 'i');
                newTag = newTag.replace(regex, '</' + replacementTag);

                $(this).replaceWith(newTag);
            });
        });*/
    }
    /*********************************/
    /**********formatTag function replace tags like td, p with span**********/
     var replacementTag = 'span';
        function formatTag(fieldID){
            $('#'+fieldID).find('td').each(function() {
                $(this ).attr("class","ansListItem");
                var outer = this.outerHTML;
                // Replace opening tag
                var regex = new RegExp('<' + this.tagName, 'i');
                var newTag = outer.replace(regex, '<' + replacementTag);

                // Replace closing tag
                regex = new RegExp('</' + this.tagName, 'i');
                newTag = newTag.replace(regex, '</' + replacementTag);

                $(this).replaceWith(newTag);
            });
            $("span.ansListItem").find('p').each(function() {
                var outer = this.outerHTML;
                // Replace opening tag
                var regex = new RegExp('<' + this.tagName, 'i');
                var newTag = outer.replace(regex, '<' + replacementTag);

                // Replace closing tag
                regex = new RegExp('</' + this.tagName, 'i');
                newTag = newTag.replace(regex, '</' + replacementTag);

                $(this).replaceWith(newTag);
            });
        }


     /****this is a just a demo function****/
     var htmlarr;
     function tryRange() {
          var html = "";
          var selectedText;
          if (typeof window.getSelection != "undefined") {
              var sel = window.getSelection();
              //alert("Next alert is from tryRange function ");
              var sel1 = rangy.getSelection();
              //alert("sel1 == > " + sel1);
              if (sel.rangeCount) {
                  var alert1 = "";
                  var container = document.createElement("div");
                  for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                      container.appendChild(sel.getRangeAt(i).cloneContents());
                      selectedText = rangy.getSelection().toHtml();
                      //alert(" asach " + asach);
                      htmlarr = selectedText.split('</td>');
                      alert1 = alert1 +'<p>'+sel.getRangeAt(i)+'</p>';
                  }
                  //console.log(htmlarr);
                  //console.log(" ARRAY SIZE "+htmlarr.length);
                  $.each( htmlarr, function( key, value ) {
                  if(value.indexOf("<td ") >= 0)
                        htmlarr[key] = value + "</td>";
                  else{
                        //console.log(" deleted value: "+value);
                        htmlarr.splice(key, 1);
                        //console.log(" deleted value: "+value);
                  }
                    //console.log("value: "+value);
                  });

                  $.each( htmlarr, function( key, value ) {
                      //console.log(" changed value: "+value);
                  });

                  $.each( htmlarr, function( key, value ) {
                       //var temp = value.innerHTML;
                       //console.log("inner html "+temp);
                       console.log("*****this key starts*****"+key );
                       var i;
                           var str2DOMElement = convertToDom(value);
                       var tags = str2DOMElement.getElementsByTagName("*");
                       //console.log("tags "+tags );
                       total = tags.length;
                       /*for ( i = 0; i < total; i++ ) {
                        var fontWeight = tags[i].style.fontWeight;
                        var textDecoration = tags[i].style.textDecoration;
                        var fontStyle = tags[i].style.fontStyle;
                        console.log(" fontWeight of " + i + " th element is " + fontWeight );
                        console.log(" textDecoration of " + i + " th element is " + textDecoration );
                        console.log(" fontStyle of " + i + " th element is " + fontStyle );
                        //tags[i].removeAttribute("class");
                        //console.log("tag " + i + " " + tags[i]);
                        //tags[i].removeAttribute("style");
                        while(tags[i].attributes.length > 0)
                            tags[i].removeAttribute(tags[i].attributes[0].name);
                        if (fontWeight)
                            tags[i].style.fontWeight = fontWeight;
                        if (textDecoration)
                            tags[i].style.textDecoration = textDecoration;
                        if (fontStyle)
                            tags[i].style.fontStyle = fontStyle;
                        var target = tags[i];
                        var wrap = document.createElement('div');
                        wrap.appendChild(target.cloneNode(true));
                        console.log("tag html "+wrap.outerHTML);
                       }*/
                      /* var str2DOMElement = convertToDom(value);
                       console.log("str2DOMElement  "+str2DOMElement);
                       var ch = str2DOMElement.childNodes;
                       console.log(" ch value for ch[0]: "+ch[0]);*/
                        var tempHtml = "<span class='tempSpan'>"+value+"</span>";
                       /********************/
                        /*$.each(value,function( index, element ) {
                            // element == this

                            //var temp = $( this ).html();
                            console.log("element : "+element);
                            *//*$( element ).css( "backgroundColor", "yellow" );*//*
                            *//*if ( $( this ).is( "#stop" ) ) {
                              $( "span" ).text( "Stopped at div index #" + index );
                            }*//*
                          });*/
                       /********************/

                       console.log("*****this key ends*****"+key);
                  });
                  html = alert1;
              }
          }
              //return html;
     }

     function removeAllAttrs(element) {
         for (var i= element.attributes.length; i-->0;)
             element.removeAttributeNode(element.attributes[i]);
     }


     function getQID(){
         var html = getSelectionText();
         document.getElementById("qidField").innerHTML = html;
     }
     function getQTitle(){
         var html = getSelectionText();
         document.getElementById("qTitleField").innerHTML = html;
     }
     function getQText(){
         var html = getSelectionParagraph();
         document.getElementById("qTextField").innerHTML = html;
     }
     function getQInstruction(){
         var html = getSelectionParagraph();
         document.getElementById("qInstructionField").innerHTML = html;
     }
     function getAnsList(){
         //tryRange();
         var htmlArray = getSelectionHtmlList();
         //console.log("htmlTExt " + html);
         //document.getElementById("ansList").outerHTML = html;
         var htmlString;
         $.each( htmlArray, function( key, value ) {
            var str;
            //var re = new RegExp("<td","g");
            //var re2 = new RegExp("</td>","g");
            //value = value.replace(re, "<span");
            //value = value.replace(re2, "</span>");
            //console.log("before removing tags "+value);
            value=value.replace(/(\/td>)/g, "/span>");
            //console.log("closing td changed "+ value);
            value=value.replace(/(<td)/g, "<span");
            //console.log("opening td changed "+ value);
            //value = value.replace(/\+/g, '<span>');
            //value = value.replaceAll("</td>", "</span>");
            str = "<li class='ans_row'><span class='ans_option_span'>" +value+"</span><span class='ans_del_span'></span><span class='ans_code_span'></span></li>";
           //alert("value : "+value);
           htmlArray[key] = str;
           console.log(" str "+str);
           console.log(" this row value "+value);
         });
         $.each( htmlArray, function( key, value ) {
            console.log(" hmm.....");
            console.log(value);
         });

         $( "#ans_ul" ).append( htmlArray );
         //console.log(" html applied ===> "+html);
         formatForNow();
         //formatAns("ansList");
         //formatTag("ansList");
     }
     function getAnsCode(){
         var html = getSelectionHtmlList();
         //document.getElementById("ansCode").innerHTML = html;
         $( "#ansCode" ).append( html );
         formatAns("ansCode");
         formatTag("ansCode");
     }
     function getScaleList(){
         var html = getSelectionText();
         document.getElementById("scaleList").innerHTML = htm l;
     }
     function getScaleCode(){
         var html = getSelectionText();
         document.getElementById("scaleCode").innerHTML = html;
     }
      function bindt(commonArray){
         $('#act_short_keys').css('background', '#ccc');
         $('#de_act_short_keys').css('background', 'none');
         jQuery(document).bind("keydown", function(event) {
         var keycode = (event.keyCode ? event.keyCode : event.which);

         $.each(questionKeys, function(i, item) {
             if(questionKeys[i].key == String.fromCharCode(event.keyCode).toLowerCase()){
                 idValue = questionKeys[i].keyValue;
                 showType(idValue,commonArray);
                 //return false;
             }
         })


         switch (keycode) {
             case 84://t
                 getQText();
                 break;
             case 90: //z
                 getQTitle();
                 break;
             case 81://q
                 getQID();
                 break;
             case 86://v
                 getQInstruction();
                 break;
             case 65://a
                 getAnsList();
                 break;
             case 80://p
                 getAnsCode();
                 break;
             case 67://c
                 getScaleList();
                 break;
             case 79://o
                 getScaleCode();
                 break;
         }
         //return false;
         });
         /*jQuery(document).bind('keydown','t',function (evt){
             //alert("t");
             getQText();
             return false;
         });
         jQuery(document).bind('keydown','a',function (evt){
             //alert("t");
             getAnswers();
             return false;
         });*/
      }

      function unBindt(){
         $('#de_act_short_keys').css('background', '#ccc');
         $('#act_short_keys').css('background', 'none');
         //alert("unbind t");
         jQuery(document).unbind('keydown');
         //alert("unbind t");
      }