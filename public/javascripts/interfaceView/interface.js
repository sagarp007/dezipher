
    var questionKeys = [
        {"key":"i","keyValue":"info_key"},
        {"key":"s","keyValue":"single_key"},
        {"key":"m","keyValue":"multi_key"},
        {"key":"o","keyValue":"openText_key"},
        {"key":"n","keyValue":"numeric_key"},
        {"key":"e","keyValue":"openTextList_key"},
        {"key":"u","keyValue":"numericList_key"},
        {"key":"r","keyValue":"ranking_key"},
        {"key":"g","keyValue":"grid_key"},
        {"key":"x","keyValue":"multiGrid_key"},
        {"key":"3","keyValue":"dGrid_key"},
        {"key":"l","keyValue":"reusableList_key"},
    ];

    function showType(idValue,commonArray){
        //alert("idValue " + idValue );
        if(idValue!='reusableList_button'){
            $('a.question_button').css('pointer-events', 'none');
            $('a.question_button').css('cursor', 'default');
            $('.common_field').css('display', 'inline-block');
            //var idValue = $(this).attr('id');
            //console.log("idValue"+idValue);
            if(idValue=='info_button'||idValue=='info_key'){
                $('.save_question').css('display', 'inline-block');
            }else if(idValue!='info_button' && idValue!='reusableList_button'){
                var idName = idValue.split('_');
                questionSelected = idName[0];
               // console.log("questionSelected: "+questionSelected);
                //console.log("id ====> "+ id);
                //return false;
                displayFields = new Array();
                $.each(commonArray, function(index, val) {
                    if(val.type == questionSelected && val.qElementType=="field"){
                       displayFields.push(val.qElementName);
                    }
                });
                //console.log("displayFields.length-------->" + displayFields.length);
                var i = 0;
                var fieldIdValue;
                do {
                    //console.log("displayFields"+displayFields[i]);
                    //console.log("$(displayFields[i].toString())"+displayFields[i].toString());
                    var str1 = displayFields[i].toString().toLowerCase();
                    //console.log("str1"+str1);
                    var tempIdField = str1;
                    //console.log("tempIdField => this may have spaces"+ tempIdField);
                    var tempId = tempIdField.split(' ');//tempId[0]=ans tempId[1]=list
                    //console.log("tempId[0] => this splits things"+ tempId[0]);
                    var j = 0;
                    var tempIdArray = new Array();
                    fieldIdValue='';
                    do {
                        tempIdArray = tempId[j];
                        //console.log("tempId[j]------>"+tempId[j]);
                         if(fieldIdValue=='')
                            fieldIdValue = tempIdArray;//fieldIdValue = ans; fieldIdValue = anslist
                         else
                            fieldIdValue = fieldIdValue + '_' + tempIdArray;
                        //var k = 0;
                        //console.log("fieldIdValue  ......."+fieldIdValue);
                        j++;
                    }while(j < tempId.length)
                    fieldIdValue = fieldIdValue + '_field';
                    //console.log("fieldIdValue  ......."+fieldIdValue);
                    $('#'+fieldIdValue).css('display', 'inline-block');
                    i++;
                }while(i < displayFields.length)
                $('.save_question').css('display', 'inline-block');
                //console.log("displayFields[0]====>>>>>"+displayFields[0]);
            }
        }
    }
     function saveQuestion(){
         $('.question_field').css('display', 'none');
         $('a.question_button').css('pointer-events', 'auto');
         $('a.question_button').css('cursor', 'pointer');
         //$('.question_button').unbind();
     }
     function cancelQuestion(){
         $('.question_field').css('display', 'none');
         $('a.question_button').css('pointer-events', 'auto');
         $('a.question_button').css('cursor', 'pointer');
         //$('.question_button').unbind();
     }

     function getSelectionText() {
         var html = "";
         if (typeof window.getSelection != "undefined") {
             var sel = window.getSelection();
             if (sel.rangeCount) {
                 var alert1 = "";
                 var container = document.createElement("div");
                 for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                     container.appendChild(sel.getRangeAt(i).cloneContents());
                     alert1 = alert1 +'<p>'+sel.getRangeAt(i)+'</p>';
                 }
                 html = alert1;
             }
         }
         return html;
     }

     /***************for selecting paras html****************/

     function getSelectionParagraph(){
         if (typeof window.getSelection != "undefined") {
              var sel1 = rangy.getSelection();
              var selectedText = rangy.getSelection().toHtml();
              //console.log(" selectedText : "+selectedText);
         }
         return selectedText;
     }

     /*******************************/

    /***************========for ans list selection========****************/
    /*****The getSelectionHtmlList function gives an array***********
    ******of ansListItem(s) which is an array of selected***********
    ******html ranges*********************************************/
    var htmlarr1;
    function getSelectionHtmlList() {
          var htmlPara = [];
          var htmlString = "";
          var selectedText;
          if (typeof window.getSelection != "undefined") {
              var sel = rangy.getSelection();
              var paraCount = 0;
              if (sel.rangeCount) {
                  var container = document.createElement("div");
                  for (var i = 0, len = sel.rangeCount; i < len; i++) {
                      container.appendChild(sel.getRangeAt(i).cloneContents());
                      selectedText = rangy.getSelection().toHtml();
                      //console.log("selectedText "+selectedText);
                      if(selectedText.toLowerCase().indexOf("<td ") >= 0)
                        htmlarr1 = selectedText.split('</td>');
                      else{
                        htmlPara[paraCount] = "<span class='ansListItem'>"+sel.getRangeAt(i)+'</span>';
                        paraCount++;
                      }
                  }
                  if (htmlPara.length == 0) {
                      $.each( htmlarr1, function( key, value ) {
                          if(value.toLowerCase().indexOf("<td ") >= 0)
                                htmlarr1[key] = value + "</td>";
                      });
                  }
              }
          }
          if(htmlPara.length != 0)
            htmlString = htmlPara;
          else
            htmlString = htmlarr1;
          return htmlString;
    }

    /****************************************/


    /***************************************/
        var retainClassArray = ["ansListItem", "ans_option_span", "ans_del_span","ans_code_span","delete_button_icon"];
    function formatForNow(){
        $('li.ans_row').find('*').each(function() {
            var attributes = $.map(this.attributes, function(item) {
                return item.name;
              });
            var fontWeight = $(this).css('font-weight');
            var fontStyle = $(this).css('font-style');
            var textDecoration = $(this).css('text-decoration');
            var className = $(this).attr('class');

              // now use jQuery to remove the attributes
              var img = $(this);
              $.each(attributes, function(i, item) {
                img.removeAttr(item);
              });

              if(className){
                var i;
                for (i = 0; i < retainClassArray.length; ++i) {
                    if(className==retainClassArray[i])
                    //$(this ).attr("class",retainClassArray[i]);
                    $(this ).addClass(retainClassArray[i]);
                }
              }

              /*if(fontWeight){
                if (fontWeight  == 'bold' || fontWeight  == '700'){
                    //$(this ).attr("style","font-weight:bold");
                    $(this ).addClass("bold");
                }
              }

              if(fontStyle){
                if (fontStyle  == 'italic'){
                    //$(this ).css("font-style","italic");
                    $(this ).addClass("italic");
                }
              }
              if(textDecoration){
                if (textDecoration == 'underline'){
                    //$(this ).css("text-decoration","underline");
                    $(this ).addClass("underline");
                }
              }*/
        });
    }
    /***************************************/


    /*****************resetting html for bold,italics, underline, className*****************/
    function formatAns(fieldID){
        $('#'+fieldID).find('*').each(function() {
            var attributes = $.map(this.attributes, function(item) {
                return item.name;
              });
            var fontWeight = $(this).css('font-weight');
            var fontStyle = $(this).css('font-style');
            var textDecoration = $(this).css('text-decoration');
            var className = $(this).attr('class');

              // now use jQuery to remove the attributes
              var img = $(this);
              $.each(attributes, function(i, item) {
                img.removeAttr(item);
              });

              if(className){
                if(className=="ansListItem"){
                    $(this ).attr("class","ansListItem");
                }
              }

              if(fontWeight){
                if (fontWeight  == 'bold' || fontWeight  == '700'){
                    $(this ).attr("style","font-weight:bold");
                }
              }

              if(fontStyle){
                if (fontStyle  == 'italic'){
                    $(this ).css("font-style","italic");
                }
              }
              if(textDecoration){
                if (textDecoration == 'underline'){
                    $(this ).css("text-decoration","underline");
                }
              }
        });
    }
    /*********************************/
    /**********formatTag function replace tags like td, p with span**********/
     var replacementTag = 'span';
        function formatTag(fieldID){
            $('#'+fieldID).find('td').each(function() {
                $(this ).attr("class","ansListItem");
                var outer = this.outerHTML;
                // Replace opening tag
                var regex = new RegExp('<' + this.tagName, 'i');
                var newTag = outer.replace(regex, '<' + replacementTag);

                // Replace closing tag
                regex = new RegExp('</' + this.tagName, 'i');
                newTag = newTag.replace(regex, '</' + replacementTag);

                $(this).replaceWith(newTag);
            });
            $("span.ansListItem").find('p').each(function() {
                var outer = this.outerHTML;
                // Replace opening tag
                var regex = new RegExp('<' + this.tagName, 'i');
                var newTag = outer.replace(regex, '<' + replacementTag);

                // Replace closing tag
                regex = new RegExp('</' + this.tagName, 'i');
                newTag = newTag.replace(regex, '</' + replacementTag);

                $(this).replaceWith(newTag);
            });
        }


     /****this is a just a demo function****/
     var htmlarr;
     function tryRange() {
          var html = "";
          var selectedText;
          if (typeof window.getSelection != "undefined") {
              var sel = window.getSelection();
              //alert("Next alert is from tryRange function ");
              var sel1 = rangy.getSelection();
              //alert("sel1 == > " + sel1);
              if (sel.rangeCount) {
                  var alert1 = "";
                  var container = document.createElement("div");
                  for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                      container.appendChild(sel.getRangeAt(i).cloneContents());
                      selectedText = rangy.getSelection().toHtml();
                      //alert(" asach " + asach);
                      htmlarr = selectedText.split('</td>');
                      alert1 = alert1 +'<p>'+sel.getRangeAt(i)+'</p>';
                  }
                  //console.log(htmlarr);
                  //console.log(" ARRAY SIZE "+htmlarr.length);
                  $.each( htmlarr, function( key, value ) {
                  if(value.indexOf("<td ") >= 0)
                        htmlarr[key] = value + "</td>";
                  else{
                        //console.log(" deleted value: "+value);
                        htmlarr.splice(key, 1);
                        //console.log(" deleted value: "+value);
                  }
                    //console.log("value: "+value);
                  });

                  $.each( htmlarr, function( key, value ) {
                      //console.log(" changed value: "+value);
                  });

                  $.each( htmlarr, function( key, value ) {
                       //var temp = value.innerHTML;
                       //console.log("inner html "+temp);
                       //console.log("*****this key starts*****"+key );
                       var i;
                           var str2DOMElement = convertToDom(value);
                       var tags = str2DOMElement.getElementsByTagName("*");
                       //console.log("tags "+tags );
                       total = tags.length;
                        var tempHtml = "<span class='tempSpan'>"+value+"</span>";
                       /********************/

                       /********************/

                       //console.log("*****this key ends*****"+key);
                  });
                  html = alert1;
              }
          }
              //return html;
     }

     function removeAllAttrs(element) {
         for (var i= element.attributes.length; i-->0;)
             element.removeAttributeNode(element.attributes[i]);
     }


     function getQID(){
         var html = getSelectionText();
         document.getElementById("qidField").innerHTML = html;
     }
     function getQTitle(){
         var html = getSelectionText();
         document.getElementById("qTitleField").innerHTML = html;
     }
     function getQText(){
         var html = getSelectionParagraph();
         document.getElementById("qTextField").innerHTML = html;
     }
     function getQInstruction(){
         var html = getSelectionParagraph();
         document.getElementById("qInstructionField").innerHTML = html;
     }
     function getAnsList(){
         //tryRange();
         var htmlArray = getSelectionHtmlList();
         var htmlString;
         $.each( htmlArray, function( key, value ) {
           var str;
           value=value.replace(/(\/td>)/g, "/span>");
           value=value.replace(/(<td)/g, "<span");
           str = "<li class='ans_row'><span class='ans_option_span'>" +value+"</span><span class='ans_del_span'><a class='delete_button_icon'></a></span><span class='ans_code_span' contenteditable='true'></span></li>";
           htmlArray[key] = str;
           //console.log(" str "+str);
           //console.log(" this row value "+value);
         });
         $.each( htmlArray, function( key, value ) {
            //console.log(" hmm.....");
            //console.log(value);
         });
         $( "#ans_ul" ).append( htmlArray );
         formatForNow();
         //var numitems =  $("#ans_ul li").length;
         //alert("list items"+numitems);
     }
     function getAnsCode(){
         var numItems =  $("#ans_ul li.ans_row").length;
         var ansListItems = numItems;
         var arrayItems;
         var arrayItemsTemp=0;
         alert("list items"+numItems);
         var htmlArray = getSelectionHtmlList();
         var codesLength = htmlArray.length;
         if(numItems == 0){

         }else{
            $("#ans_ul li.ans_row").each(function() {
            var codeText = $(this).find("ans_code_span").text();
                if(!codeText && arrayItemsTemp<ansListItems){
                    var myContent = htmlArray[arrayItemsTemp];
                    var textTemp = $(myContent).text();
                    $(this).find(".ans_code_span").text(textTemp);
                    alert("textTemp"+textTemp);
                    arrayItemsTemp++;

                }
            });
            if(codesLength>=ansListItems){
                var i;
                var diff = codesLength - arrayItemsTemp;
                var str;
                for(i=diff;i<codesLength;i++){
                    var textTemp = $(htmlArray[i]).text();
                    str = "<li class='ans_row'><span class='ans_option_span'></span><span class='ans_del_span'><a class='delete_button_icon'></a></span><span class='ans_code_span' contenteditable='true'>" +textTemp+ "</span></li>";
                    $("#ans_ul").append( str );
                }
            }
         }
         //var html = getSelectionHtmlList();
         //document.getElementById("ansCode").innerHTML = html;
         //$( "#ansCode" ).append( html );
         //formatAns("ansCode");
         //formatTag("ansCode");
     }
     function getScaleList(){
         var html = getSelectionText();
         document.getElementById("scaleList").innerHTML = html;
     }
     function getScaleCode(){
         var html = getSelectionText();
         document.getElementById("scaleCode").innerHTML = html;
     }
      function bindt(commonArray){
         $('#act_short_keys').css('background', '#ccc');
         $('#de_act_short_keys').css('background', 'none');
         jQuery(document).bind("keydown", function(event) {
         var keycode = (event.keyCode ? event.keyCode : event.which);

         $.each(questionKeys, function(i, item) {
             if(questionKeys[i].key == String.fromCharCode(event.keyCode).toLowerCase()){
                 idValue = questionKeys[i].keyValue;
                 showType(idValue,commonArray);
                 //return false;
             }
         })
         switch (keycode) {
             case 84://t
                 getQText();
                 break;
             case 90: //z
                 getQTitle();
                 break;
             case 81://q
                 getQID();
                 break;
             case 86://v
                 getQInstruction();
                 break;
             case 65://a
                 getAnsList();
                 break;
             case 80://p
                 getAnsCode();
                 break;
             case 67://c
                 getScaleList();
                 break;
             case 79://o
                 getScaleCode();
                 break;
         }
         });
      }

      function unBindt(){
         $('#de_act_short_keys').css('background', '#ccc');
         $('#act_short_keys').css('background', 'none');
         //alert("unbind t");
         jQuery(document).unbind('keydown');
         //alert("unbind t");
      }