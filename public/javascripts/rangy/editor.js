    function gEBI(id) {
		return document.getElementById(id);
	}
	var cssApplier, boldApplier, italicApplier, underlineApplier;
	function toggleBold() {
		boldApplier.toggleSelection();
	}
	function toggleItalic() {
		italicApplier.toggleSelection();
	}
	function toggleUnderline() {
		underlineApplier.toggleSelection();
	}
    window.onload = function() {
            rangy.init();
            // Enable buttons
            var classApplierModule = rangy.modules.ClassApplier;
           /* var textCommandsModule = rangy.modules.TextCommands;
            if (rangy.supported && textCommandsModule && textCommandsModule.supported) {
                *//*var toggleBoldButton = gEBI("toggleBoldButton");
                toggleBoldButton.disabled = false;
                toggleBoldButton.ontouchstart = toggleBoldButton.onmousedown = function() {
                    //alert(rangy.querySelectionCommandValue("bold"));
                    rangy.execSelectionCommand("bold");
                    return false;
                };*//*

                var toggleItalicButton = gEBI("toggleItalicButton");
                toggleBoldButton.disabled = false;
                toggleBoldButton.ontouchstart = toggleBoldButton.onmousedown = function() {
                    //alert(rangy.querySelectionCommandValue("bold"));
                    rangy.execSelectionCommand("italic");
                    return false;
                };

                *//*var toggleUnderlineButton = gEBI("toggleUnderlineButton");
                toggleBoldButton.disabled = false;
                toggleBoldButton.ontouchstart = toggleBoldButton.onmousedown = function() {
                    //alert(rangy.querySelectionCommandValue("bold"));
                    rangy.execSelectionCommand("underline");
                    return false;
                };*//*
            }*/
            // Next line is pure paranoia: it will only return false if the browser has no support for ranges,
            // selections or TextRanges. Even IE 5 would pass this test.
            if (rangy.supported && classApplierModule && classApplierModule.supported) {
                boldApplier = rangy.createClassApplier("bold");
                italicApplier = rangy.createCssClassApplier("italic");
                underlineApplier = rangy.createCssClassApplier("underline");

				var toggleBoldButton = gEBI("toggleBold");
                toggleBoldButton.disabled = false;
                toggleBoldButton.ontouchstart = toggleBoldButton.onmousedown = function() {
                    toggleBold();
                    return false;
                };

				var toggleItalicButton = gEBI("toggleItalic");
                toggleItalicButton.disabled = false;
                toggleItalicButton.ontouchstart = toggleItalicButton.onmousedown = function() {
                    toggleItalic();
                    return false;
                };

				var toggleUnderlineButton = gEBI("toggleUnderline");
                toggleUnderlineButton.disabled = false;
                toggleUnderlineButton.ontouchstart = toggleUnderlineButton.onmousedown = function() {
                    toggleUnderline();
                    return false;
                };

            }
        };
	function getHtml() {
		//alert("hi");
		/*$('span.boldRed').replaceWith(function(){
			$( this ).replaceWith( "<div>" + $( this ).text() + "</div>" );//return $("<b/>", {html: $(this).html()});
		});*/
		$('span.bold').replaceWith(function(){
			return $("<b/>").append($(this).contents());
		});
		//$( "span.boldRed" ).replaceWith( $( "div" ) );
		var htmlString = $( "span.qText_field" ).html();
		//alert(" htmlString : "+htmlString);
	}